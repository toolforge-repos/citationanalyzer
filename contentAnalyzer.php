<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST["articleURL"]) && $_POST["articleURL"] != "") {
        $articleURL = $_POST["articleURL"];
        $wikiTextURL = $articleURL . "?action=raw";
        $wikiText = file_get_contents($wikiTextURL);

        // Count the number of citations
        $bookCitations = preg_match_all('/{{cite book/i', $wikiText, $bookMatches);
        $webCitations = preg_match_all('/{{cite web/i', $wikiText, $webMatches);
        $newsCitations = preg_match_all('/{{cite news/i', $wikiText, $newsMatches);
        $magazineCitations = preg_match_all('/{{cite magazine/i', $wikiText, $magazineMatches);
        $avMediaCitations = preg_match_all('/{{cite AV media/i', $wikiText, $avMediaMatches);
        $journalCitations = preg_match_all('/{{cite journal/i', $wikiText, $journalMatches);
        $sfnCitations = preg_match_all('/{{sfn/i', $wikiText, $sfnMatches);
        
        $citationCount = count($bookMatches[0]) + count($webMatches[0]) + count($newsMatches[0]) + count($magazineMatches[0]) + count($avMediaMatches[0]) + count($journalMatches[0]) + count($sfnMatches[0]);

        // Count the total number of words
        $wikiApiURL = "https://en.wikipedia.org/w/api.php?action=query&prop=extracts&exlimit=1&explaintext=1&format=json&formatversion=2&titles=" . urlencode(basename($articleURL)) . "&explaintext=1";
        $wikiData = file_get_contents($wikiApiURL);
        $wikiData = json_decode($wikiData, true);
        $pageID = key($wikiData['query']['pages']);
        $wikiText = $wikiData['query']['pages'][$pageID]['extract'];
        $wordCount = str_word_count(strip_tags($wikiText));

        // Evaluate the citation ratio
        $wordsPerCitation = $wordCount / $citationCount;

        // Evaluate article based on citation ratio
        if ($wordsPerCitation <= 50) {
            $evaluation = "High amount of references.";
        } elseif ($wordsPerCitation <= 100) {
            $evaluation = "Average amount of references.";
        } elseif ($wordsPerCitation <= 200) {
            $evaluation = "Low amount of references.";
        } else {
            $evaluation = "Very low amount of references.";
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Citation Analyzer Result</title>
    <link rel="stylesheet" type="text/css" href="phpstyles.css">
</head>
<body>
    <h1>Citation Analyzer Result</h1>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        if(isset($_POST["articleURL"]) && $_POST["articleURL"] != "") {
    ?>
    <div class="result">
        <p>Article URL: <?php echo $articleURL; ?></p>
        <p>Book Citations: <?php echo $bookCitations; ?></p>
        <p>Web Citations: <?php echo $webCitations; ?></p>
        <p>News Citations: <?php echo $newsCitations; ?></p>
        <p>Magazine Citations: <?php echo $magazineCitations; ?></p>
        <p>AV Media Citations: <?php echo $avMediaCitations; ?></p>
        <p>Journal Citations: <?php echo $journalCitations; ?></p>
        <p>Footnote Citations: <?php echo $sfnCitations; ?></p>
        <p>Total Citations: <?php echo $citationCount; ?></p>
        <p>Total Words: <?php echo $wordCount; ?></p>
        <p>Words per Citation: <?php echo $wordsPerCitation; ?></p>
        <p>Article Evaluation: <?php echo $evaluation; ?></p>
    </div>
    <?php
        } else {
    ?>
    <div class="result">
        <p class="evaluation">Please input a URL or article title.</p>
    </div>
    <?php
        }
    }
    ?>
</body>
</html>

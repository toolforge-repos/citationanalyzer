<?php
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES["image"]) && isset($_POST["watermark"])) {

    $watermark = $_POST["watermark"];

    // Execute lib.py to embed the watermark
    $command = "python lib.py " . escapeshellarg($watermark);
    $output = shell_exec($command);

    // Execute libe.py to extract the watermark
    $command_e = "python libe.py";
    $output_e = shell_exec($command_e);

    echo "<pre>$output_e</pre>";
}

if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_FILES["watermarked_image"])) {

    // Execute libe.py to extract the watermark
    $command_e = "python libe.py";
    $output_e = shell_exec($command_e);
    
    echo "<pre>$output_e</pre>";
}
?>

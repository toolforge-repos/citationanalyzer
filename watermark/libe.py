from blind_watermark import WaterMark

# Create a WaterMark object
bwm1 = WaterMark(password_img=1, password_wm=1)

# Read the watermarked image
wm_extract = bwm1.extract('embedded.png', wm_shape=int(open("len_wm.txt", "r").read()), mode='str')

# Print the extracted watermark
print(wm_extract)

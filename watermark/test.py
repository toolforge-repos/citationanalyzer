import cv2
from imwatermark import WatermarkEncoder

bgr = cv2.imread('test.png')
wm = '@ThePalestineTimes'

encoder = WatermarkEncoder()
encoder.set_watermark('bytes', wm.encode('utf-8'))
bgr_encoded = encoder.encode(bgr, 'dwtDct')

cv2.imwrite('test_wm.png', bgr_encoded)
from blind_watermark import WaterMark
import sys

# Read the watermark from the command-line arguments
wm = sys.argv[1]

# Create a WaterMark object
bwm1 = WaterMark(password_img=1, password_wm=1)

# Read the image
bwm1.read_img('test.png')

# Read the watermark
bwm1.read_wm(wm, mode='str')

# Embed the watermark into the image
bwm1.embed('embedded.png')

# Calculate the length of the watermark
len_wm = len(bwm1.wm_bit)

# Print the length of the watermark
print('Put down the length of wm_bit {len_wm}'.format(len_wm=len_wm))

# Save the length of the watermark to a text file
with open('len_wm.txt', 'w') as f:
    f.write(str(len_wm))

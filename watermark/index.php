<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Watermark Embedding and Extraction</title>
</head>
<body>
    <h1>Watermark Embedding and Extraction</h1>

    <form action="process.php" method="post" enctype="multipart/form-data">
        <h2>Embed Watermark</h2>
        <input type="file" name="image" required><br><br>
        <input type="text" name="watermark" placeholder="Enter watermark" required><br><br>
        <input type="submit" value="Embed Watermark">
    </form>

    <form action="process.php" method="post" enctype="multipart/form-data">
        <h2>Extract Watermark</h2>
        <input type="file" name="watermarked_image" required><br><br>
        <input type="submit" value="Extract Watermark">
    </form>
</body>
</html>
